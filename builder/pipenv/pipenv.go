package pipenv

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser/pipdeptree"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser/piplock"
)

const (
	pathPipenv = "pipenv"
)

var _ builder.Builder = (*Builder)(nil)

// Builder generates dependency lists for pipenv projects
type Builder struct{}

// Build generates a dependency list for a Pipfile, and returns its path
func (b Builder) Build(input string) (string, []string, error) {
	dir := filepath.Dir(input)

	_, err := os.Stat(filepath.Join(dir, piplock.FilePipfileLock))
	output := filepath.Join(dir, piplock.FilePipenvProjectPackagesJSON)
	if err == nil {
		if err := b.sync(dir); err != nil {
			return "", nil, fmt.Errorf("syncing dependencies %s: %w", output, err)
		}
	} else {
		if err := b.install(dir); err != nil {
			return "", nil, fmt.Errorf("installing dependencies %s: %w", output, err)
		}
	}

	if err := b.fixDepNames(dir); err != nil {
		return "", nil, err
	}

	return output, nil, nil
}

// sync strictly installs the dependencies in a Pipfile.lock and
// does not attempt to update or edit the lock file in any way.
func (Builder) sync(dir string) error {
	sync := func(force bool) error {
		args := []string{"sync", "--verbose", "--dev"}
		if force {
			log.Warn("pipenv sync failed, trying again with forced python path")
			args = append(args, "--python", "python3")
		}
		cmd := exec.Command(pathPipenv, args...)
		cmd.Dir = dir
		cmd.Env = os.Environ()
		out, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), out)
		if err != nil {
			return fmt.Errorf("running %s: %w", cmd.String(), err)
		}
		return nil
	}
	if err := sync(false); err != nil {
		return sync(true)
	}
	return nil
}

// install installs the dependencies declared in a Pipfile and
// generates or updates a Pipfile.lock for the given project.
// Running this function has side effects and should ONLY be
// used when a Pipfile.lock does not already exist for a project.
func (Builder) install(dir string) error {
	install := func(force bool) error {
		args := []string{"install", "--verbose", "--dev"}
		if force {
			log.Warn("pipenv install failed, trying again with forced python path")
			args = append(args, "--python", "python3")
		}
		cmd := exec.Command(pathPipenv, args...)
		cmd.Dir = dir
		cmd.Env = os.Environ()
		out, err := cmd.CombinedOutput()
		if err != nil {
			return fmt.Errorf("running %s: %w", cmd.String(), err)
		}
		log.Debugf("%s\n%s", cmd.String(), out)
		return nil
	}
	if err := install(false); err != nil {
		return install(true)
	}
	return nil
}

// fixDepNames is a hack to force the packages in a Pipfile.lock
// file to match those identified by `pipenv graph`. This exists
// to prevent the creation of duplicate findings in the move from
// graph output scanning to lock file scanning.
// More information at https://gitlab.com/gitlab-org/gitlab/-/issues/375505#note_1373983810
func (Builder) fixDepNames(dir string) error {
	input := filepath.Join(dir, piplock.FilePipfileLock)
	output := filepath.Join(dir, piplock.FilePipenvProjectPackagesJSON)
	log.Debugf("Fixing package names used by Pipenv project in %s", dir)
	pkgNameOverrides, err := overrides(dir)
	if err != nil {
		return err
	}

	pipfileLock, err := readPipfileLock(input)
	if err != nil {
		return err
	}

	for old, new := range pkgNameOverrides {
		log.Debugf("Fixing pipenv package name old: %s new: %s", old, new)
		if pkg, ok := pipfileLock.Default[old]; ok {
			pipfileLock.Default[new] = pkg
			delete(pipfileLock.Default, old)
		}
		if pkg, ok := pipfileLock.Develop[old]; ok {
			pipfileLock.Develop[new] = pkg
			delete(pipfileLock.Develop, old)
		}
	}

	err = writePipfileLock(output, pipfileLock)
	if err != nil {
		return err
	}
	log.Debugf("Fixed package names used by Pipenv project in %s", dir)
	return nil
}

// overrides produces a map that contains the canonical names with their
// corresponding normalized names
func overrides(dir string) (map[string]string, error) {
	// We separate the stdout and stderr streams so that
	// any warnings produced on stderr do not cause the
	// json graph output to become malformed.
	var (
		stdout bytes.Buffer
		stderr bytes.Buffer
	)
	cmd := exec.Command("pipenv", "graph", "--json")
	cmd.Dir = dir
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		log.Debugf("%s\n%s\n%s", cmd.String(), stdout.String(), stderr.String())
		return nil, fmt.Errorf("running %s: %w", cmd.String(), err)
	}
	log.Debugf("%s\n%s", cmd.String(), stdout.String())
	var graph []struct {
		Package pipdeptree.Package `json:"package"`
	}
	if err := json.Unmarshal(stdout.Bytes(), &graph); err != nil {
		return nil, fmt.Errorf("running %s: %w", cmd.String(), err)
	}

	out := make(map[string]string)
	for _, node := range graph {
		if node.Package.Key == node.Package.Name {
			continue
		}
		out[node.Package.Key] = node.Package.Name
	}
	return out, nil
}

// readPipfileLock reads the Pipfile.lock file at a given path.
func readPipfileLock(path string) (*piplock.Document, error) {
	log.Debugf("Reading Pipfile.lock in %s", path)
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Errorf("Closing file handle for %s: %v", path, err)
		}
	}()

	var out piplock.Document
	if err := json.NewDecoder(f).Decode(&out); err != nil {
		return nil, fmt.Errorf("parsing pipfile packages: %w", err)
	}
	log.Debugf("Reading Pipfile.lock in %s", path)
	return &out, nil
}

// writePipfileLock writes a modified Pipfile.lock file at a given path.
func writePipfileLock(path string, doc *piplock.Document) error {
	log.Debugf("Saving modified Pipfile.lock in %s", path)
	f, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0o644)
	if err != nil {
		return fmt.Errorf("overwriting contents of %s: %w", path, err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Errorf("Closing file handle for %s: %v", path, err)
		}
	}()

	enc := json.NewEncoder(f)
	enc.SetIndent("", "    ")
	enc.SetEscapeHTML(false)
	if err := enc.Encode(doc); err != nil {
		return fmt.Errorf("encoding normalized dependencies into pipfile.lock file handle: %w", err)
	}
	log.Debugf("Saved modified Pipfile.lock in %s", path)
	return nil
}

func init() {
	builder.Register("pipenv", &Builder{})
}
