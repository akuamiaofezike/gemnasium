package remediate

import (
	"context"
	"path"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner"
)

// ErrFileNotCurable is raised when the affected dependency file is not supported
type ErrFileNotCurable struct {
	filepath string
}

func (e ErrFileNotCurable) Error() string {
	return "Cannot auto-remediate dependency file, not supported: " + e.filepath
}

// Cure combines affections with their solution.
type Cure struct {
	Summary    string
	Diff       []byte
	Affections []scanner.Affection
}

// Remediate returns the cures for an affected dependency file.
func Remediate(ctx context.Context, file scanner.File) ([]Cure, error) {
	// TODO implement plugins and plugin registry
	switch path.Base(file.Path) {
	case "yarn.lock":
		return cureYarnLock(ctx, file)
	}
	return nil, ErrFileNotCurable{file.Path}
}
