workflow:
 rules:
   # When a new tag is created, run a pipeline.
   - if: $CI_COMMIT_TAG
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, manually triggering a master pipeline, etc.).
   - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # For version branches, create a pipeline.
   - if: '$CI_COMMIT_BRANCH =~ /^v\d$/'
    # For merge requests, create a pipeline.
   - if: $CI_MERGE_REQUEST_ID

variables:
  # SETTINGS
  #
  # Language of the SBOM generator tool, like "golang", "ruby", "javascript".
  #
  # This is used as follows:
  #
  # 1. The name of the image is published to registry.gitlab.com/security-products/sbomgen/<language>:
  #
  #    Example: registry.gitlab.com/security-products/sbomgen/golang
  #
  # 2. The name of the directory that contains the Dockerfile is located in build/sbomgen-<language>:
  #
  #    Example: build/sbomgen-golang/Dockerfile
  #
  # 3. The temporary image name gemnasium/tmp/sbomgen-<language>
  #
  #    Example: gemnasium/tmp/sbomgen-golang
  #
  # 4. The local image name gemnasium/sbomgen-<language>
  #
  #    Example: gemnasium/sbomgen-golang
  #
  # 5. The name of the executable sbomgen-<language> used for generating the SBOM
  #
  #    Example: sbomgen-golang
  #
  LANGUAGE: ""

  # SHARED VARIABLES
  TMP_IMAGE_PATH: "/tmp/sbomgen-$LANGUAGE"
  IMAGE_PATH: "/sbomgen-$LANGUAGE"
  SEC_REGISTRY_IMAGE: "registry.gitlab.com/security-products/sbomgen/$LANGUAGE"
  SEC_REGISTRY_USER: "$SBOMGEN_SEC_REGISTRY_USER"
  SEC_REGISTRY_PASSWORD: "$SBOMGEN_SEC_REGISTRY_PASSWORD"
  # EXECUTABLE_PATH is used in the `check analyzer version` test in https://gitlab.com/gitlab-org/security-products/ci-templates/-/blob/2b7fad2a/includes-dev/docker-test.yml#L94-94
  EXECUTABLE_PATH: "sbomgen-$LANGUAGE"

stages:
  - build-image
  - test
  - release-version
  - release-major

include:
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/docker.yml'
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/docker-test.yml'

build tmp image:
  variables:
    DOCKERFILE: "build/sbomgen-$LANGUAGE/alpine/Dockerfile"

build tmp image fips:
  variables:
    DOCKERFILE: "build/sbomgen-$LANGUAGE/redhat/Dockerfile"

# Disable container scanning because sbomgen tool images are experimental and
# we don't want to add to the existing vulnerability findings.
# TODO: enable the container scanning jobs once the sbomgen tools are fully supported
container_scanning:
  rules:
    - when: never

container_scanning-fips:
  rules:
    - when: never

image test:
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/integration-test:stable
  stage: test
  timeout: 5m
  services:
    - docker:20.10-dind
  variables:
    IMAGE_TAG_SUFFIX: ""
    TMP_IMAGE: "$CI_REGISTRY_IMAGE$TMP_IMAGE_PATH:$CI_COMMIT_SHA$IMAGE_TAG_SUFFIX"
  script:
    - rspec -f d spec/sbomgen-${LANGUAGE}_image_spec.rb
  artifacts:
    when: always
    paths:
      - tmp/test-*/**/gl-sbom-*.cdx.json

image test fips:
  extends: image test
  variables:
    IMAGE_TAG_SUFFIX: "-fips"

# TODO: re-enable this test as part of https://gitlab.com/gitlab-org/gitlab/-/issues/370025
test-custom-ca-bundle:
  rules:
    - when: never

# TODO: re-enable this test as part of https://gitlab.com/gitlab-org/gitlab/-/issues/370025
test-custom-ca-bundle fips:
  rules:
    - when: never
