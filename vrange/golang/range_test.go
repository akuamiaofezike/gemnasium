package golang

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/advisory"
)

func isExpected(expectedResult bool) string {
	if !expectedResult {
		return " not"
	}

	return ""
}

func TestSatisfiesRangeConstraint(t *testing.T) {
	type RangeCheck struct {
		constraint     string
		version        string
		expectedResult bool
	}

	for _, tt := range []RangeCheck{
		{
			"<=v5.4.3 || >=v1.2.3",
			"v1.2.3",
			true,
		},
		{
			">1.0.0 <2.3.5", "2.3.5",
			false,
		},
		{
			">1.0.0-alpha <=2.3.5", "1.0.0",
			true,
		},
		{
			"<2.6.9", "4.5.1",
			false,
		},
		{
			">=4.5.1 <=4.5.5", "4.5.1",
			true,
		},
		{
			">= 2.0.0 <= 2.0.0 ", "2.0.0",
			true,
		},
		{
			">=v1.7.2 <=v1.7.3", "v1.7.4",
			false,
		},
		{
			"<v0.0.0-20191112214715-2bda9f819ef6", "v0.0.0-20131112214715-2bda9f819ef6",
			true,
		},
		{
			">v0.0.0-20181112214715-2bda9f819ef6 <v0.0.0-20191112214715-2bda9f819ef6", "v0.0.0-20131112214715-2bda9f819ef6",
			false,
		},
	} {
		result, err := SatisfiesRangeConstraint(tt.constraint, tt.version)
		require.NoErrorf(t, err, "unexpected error for %s, %s", tt.constraint, tt.version)
		require.Equalf(t, result, tt.expectedResult, "%s is%s supposed to satisfy the range constraint %s", tt.version,
			isExpected(tt.expectedResult), tt.constraint)
	}
}

func TestRewriteConstraint(t *testing.T) {
	for _, tt := range []struct {
		in             string
		expectedResult string
	}{
		{
			"<v0.0.0-20191112214715-2bda9f819ef6",
			"<v20191112214715.0.0",
		},
		{
			"<v0.0.0-20191112214715-2bda9f819ef6 || >v0.0.0-20201112214715-2bda9f819ef6 <v0.0.0-20241112214715-2bda9f819ef6",
			"<v20191112214715.0.0||>v20201112214715.0.0 <v20241112214715.0.0",
		},
	} {
		result, err := rewriteRangeConstraint(tt.in)
		require.NoErrorf(t, err, "unexpected error for %s", tt.in)
		require.Equal(t, tt.expectedResult, result)
	}
}

func TestIsPseudoVersion(t *testing.T) {
	for _, tt := range []struct {
		in             string
		expectedResult bool
	}{
		{
			"v0.0.0-20191112214715-2bda9f819ef6",
			true,
		},
		{
			"v0.0.0-20191112214715-2fffffffffff",
			true,
		},
		{
			"v0.0.0-20191112214715-2fffffffffff",
			true,
		},
		{
			"v1.0.0-20191112214715-2bda9f819ef6",
			false,
		},
		{
			"v1.0.0",
			false,
		},
	} {
		got := IsPseudoVersion(tt.in)
		require.Equalf(t, tt.expectedResult, got, "%s is%s supposed to be a pseudo-version", tt.in, isExpected(tt.expectedResult))
	}
}

func TestExtractVersion(t *testing.T) {
	for _, tt := range []struct {
		in             string
		expectedResult string
	}{
		{
			"=v1.10.0",
			"v1.10.0",
		},
		{
			">=v0.0.0-20180721095515-a09bafbf2ab4",
			"v0.0.0-20180721095515-a09bafbf2ab4",
		},
	} {
		got := extractVersion(tt.in)
		require.Equal(t, tt.expectedResult, got)
	}
}

func TestIsUniform(t *testing.T) {
	for _, tt := range []struct {
		in             []string
		expectedResult bool
	}{
		{
			[]string{"=v1.10.0||=v0.0.0-20180721095515-a09bafbf2ab4", "v1.10.0"},
			false,
		},
		{
			[]string{"=v1.10.0||=v0.0.0", "1.1.1"},
			true,
		},
		{
			[]string{"=v0.0.0-20180721095515-a09bafbf2ab4||>=v0.0.0-20180721095515-a09bafbf2ab4||<=v0.0.0-20200721095515-a09bafbf2ab4", "v0.0.0-20180721095515-a09bafbf2ab4"},
			true,
		},
		{
			[]string{">=v0.0.0-20180721095515-a09bafbf2ab4", "v1.1"},
			false,
		},
		{
			[]string{"<=v1.1.4||<=v0.0.0-20180721095515-a09bafbf2ab4", "1.1.1"},
			false,
		},
	} {
		got := IsUniform(tt.in[0], tt.in[1])
		require.Equalf(t, tt.expectedResult, got, "%s, %s is%s supposed to be uniform", tt.in[0], tt.in[1], isExpected(tt.expectedResult))
	}
}

func TestGenerateRangeString(t *testing.T) {
	for _, tt := range []struct {
		in             [][]string
		expectedResult string
	}{
		{
			[][]string{{">v1.0", "<v2.0"}, {"<v3.0"}},
			">v1.0 <v2.0||<v3.0",
		},
		{
			[][]string{{">v1.0.1", "<=v3.2.1"}, {">=v4.0.1", "<4.0.2"}, {">=5.0.0", "<=6.0.0"}},
			">v1.0.1 <=v3.2.1||>=v4.0.1 <4.0.2||>=5.0.0 <=6.0.0",
		},
	} {
		got := generateRangeString(tt.in)
		require.Equal(t, tt.expectedResult, got)
	}
}

func TestTranslateToPseudoVersion(t *testing.T) {
	for _, tt := range []struct {
		in             advisory.Commit
		expectedResult string
	}{
		{
			advisory.Commit{
				Sha:       "fc82aa6cfdb437dabf18c606ed23129e5973b5ea",
				Timestamp: "20190304035542",
				Tags: []string{
					"v2019.03.04.00",
				},
			},
			"v0.0.0-20190304035542-fc82aa6cfdb4",
		},
		{
			advisory.Commit{
				Sha:       "fc82aa",
				Timestamp: "20190304035542",
				Tags: []string{
					"v2019.03.04.00",
				},
			},
			"err",
		},
	} {
		result, err := translateToPseudoVersion(tt.in)
		if tt.expectedResult == "err" {
			require.Errorf(t, err, "translation is supposed to fail for %s", tt.in.Timestamp)
			return
		}
		require.Equal(t, tt.expectedResult, result)
	}
}

func TestRangeConstraintTranslation(t *testing.T) {
	versionMetaInfo := []advisory.Version{
		{
			Number: "v2019.03.04.00",
			Commit: advisory.Commit{
				Tags: []string{
					"v2019.03.04.00",
				},
				Sha:       "fc82aa6cfdb437dabf18c606ed23129e5973b5ea",
				Timestamp: "20190304035542",
			},
		},
	}

	type expectedResult struct {
		sem    string
		pseudo string
	}

	for _, tt := range []struct {
		in             string
		expectedResult expectedResult
	}{
		{
			"<v2019.03.04.00",
			expectedResult{
				"",
				"<v0.0.0-20190304035542-fc82aa6cfdb4",
			},
		},
		{
			">v1.2.1 <v3.5.4 || <v0.0.0-20190304035542-fc82aa6cfdb4",
			expectedResult{
				">v1.2.1 <v3.5.4",
				"<v0.0.0-20190304035542-fc82aa6cfdb4",
			},
		},
	} {
		sem, pseudo, err := TranslateRangeConstraint(tt.in, versionMetaInfo)
		require.NoError(t, err)
		require.Equal(t, tt.expectedResult.sem, sem)
		require.Equal(t, tt.expectedResult.pseudo, pseudo)
	}
}
