package sbomscanner

import (
	"fmt"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
)

// Scanner is an SBOM project scanner
type Scanner struct {
	parserOpts parser.Options
}

// NewScanner parses command line arguments from a cli.Context and returns a new SBOM scanner
func NewScanner(c *cli.Context) (*Scanner, error) {
	return &Scanner{}, nil
}

// ScanProjects scans projects and returns a description of each file it has scanned.
// This includes the dependencies found in the file.
func (s Scanner) ScanProjects(dir string, projects []finder.Project) ([]scanner.File, error) {
	result := []scanner.File{}
	for _, project := range projects {
		scannable, isScannable := project.ScannableFile()
		if !isScannable {
			// skip project
			log.Debugf("skip project: no scannable file found in %s", project.Dir)
			continue
		}

		// relative and absolute path of scannable file
		rel := project.FilePath(scannable)
		path := filepath.Join(dir, rel)

		// set location to relative path of scannable file
		location := rel
		if !scannable.Linkable() {
			// update location with the relative path of the requirements file
			if requirements, ok := project.RequirementsFile(); ok {
				location = project.FilePath(requirements)
			}
		}

		// scan dependency file
		file := scanner.File{
			RootDir:        dir,
			Path:           location,
			PackageManager: project.PackageManager.Name,
		}
		if err := s.scanFile(path, &file); err != nil {
			return nil, err
		}
		result = append(result, file)
	}

	return result, nil
}

// scanFile opens, parses, and scans a dependency file with a given path.
// It updates the packages, dependencies, and package type of the given File struct.
func (s Scanner) scanFile(path string, file *scanner.File) error {
	// open file
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	// find file parser
	basename := filepath.Base(path)
	depParser := parser.Lookup(basename)
	if depParser == nil {
		return fmt.Errorf("no file parser for: %s", path)
	}
	pkgType := string(depParser.PackageType)
	file.PackageType = pkgType

	// parse file
	pkgs, deps, err := depParser.Parse(f, s.parserOpts)
	if err != nil {
		return err
	}
	file.Packages = pkgs
	file.Dependencies = deps

	return nil
}
