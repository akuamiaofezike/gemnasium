package vrange

import (
	"sort"
	"sync"

	"github.com/urfave/cli/v2"
)

const (
	flagVrangeDir   = "vrange-dir"
	envVarVrangeDir = "VRANGE_DIR"
)

var resolversMu sync.Mutex
var resolvers = make(map[string]Resolver)

// Register registers a resolver with a name.
func Register(name string, resolver Resolver) {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	if _, dup := resolvers[name]; dup {
		panic("Register called twice for name " + name)
	}
	resolvers[name] = resolver
}

// Lookup looks for a resolver with given name.
func Lookup(name string) Resolver {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	resolver, _ := resolvers[name]
	return resolver
}

// Resolvers returns a sorted list of the names of the registered resolvers.
func Resolvers() []string {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	list := make([]string, 0, len(resolvers))
	for name := range resolvers {
		list = append(list, name)
	}
	sort.Strings(list)
	return list
}

// Flags returns the flags of all configurable resolvers
func Flags() []cli.Flag {
	resolversMu.Lock()
	defer resolversMu.Unlock()
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:    flagVrangeDir,
			Usage:   "Base directory of vrange scripts",
			EnvVars: []string{envVarVrangeDir},
		},
	}
	for _, b := range resolvers {
		if cb, ok := b.(Configurable); ok {
			flags = append(flags, cb.Flags()...)
		}
	}
	return flags
}

// Configure configures all configurable resolvers
func Configure(c *cli.Context) error {
	opts := Options{
		BaseDir: c.String(flagVrangeDir),
	}
	resolversMu.Lock()
	defer resolversMu.Unlock()
	for _, b := range resolvers {
		if cb, ok := b.(Configurable); ok {
			if err := cb.Configure(c, opts); err != nil {
				return err
			}
		}
	}
	return nil
}
