package gradle

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/builder/exportpath"
)

const (
	pathDefaultGradleBinary = "/opt/asdf/shims/gradle"

	flagGradleOpts       = "gradle-opts"
	flagGradleInitScript = "gradle-init-script"
)

var errNoGradleWrapper = errors.New("cannot access gradle wrapper")

// Builder generates dependency lists for gradle projects
type Builder struct {
	GradleOpts       string
	GradleInitScript string
}

// Flags returns the CLI flags that configure the gradle command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagGradleOpts,
			Usage:   "Optional CLI arguments for the gradle dependency dump command",
			Value:   "",
			EnvVars: []string{"GRADLE_CLI_OPTS"},
		},
		&cli.StringFlag{
			Name:    flagGradleInitScript,
			Usage:   "Optional CLI argument pointing to the init script for gemnasium-gradle-plugin",
			Value:   "gemnasium-gradle-plugin-init.gradle",
			EnvVars: []string{"GRADLE_PLUGIN_INIT_PATH"},
		},
	}
}

// Configure configures the gradle command
func (b *Builder) Configure(c *cli.Context) error {
	b.GradleOpts = c.String(flagGradleOpts)
	b.GradleInitScript = c.String(flagGradleInitScript)
	return nil
}

// Build generates graph exports for all projects of a multi-project Gradle build.
// It returns the export path for the root project, and the export paths for its sub-projects, if any.
func (b Builder) Build(input string) (string, []string, error) {
	paths, err := b.listDeps(input)
	if err != nil {
		return "", []string{}, err
	}

	return exportpath.Split(paths, filepath.Dir(input))
}

// listDeps exports the dependency list to JSON using the Gemnasium Gradle Plugin.
// It returns the paths to the JSON export created for the root project and its sub-projects, if any.
func (b Builder) listDeps(input string) ([]string, error) {
	// find gradle wrapper
	dir := filepath.Dir(input)
	gradlewPath := filepath.Join(dir, "gradlew")
	if _, err := os.Stat(gradlewPath); err != nil {
		gradlewPath = pathDefaultGradleBinary
	}

	// run gemnasiumDumpDependencies gradle task
	args := strings.Fields(b.GradleOpts)
	args = append(args, "--init-script", b.GradleInitScript, "gemnasiumDumpDependencies")
	cmd := exec.Command(gradlewPath, args...)
	cmd.Dir = dir
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return exportpath.ExtractGradle(output)
}

func init() {
	builder.Register("gradle", &Builder{})
}
