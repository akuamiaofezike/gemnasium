package gemfile

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser/testutil"
)

func TestGemfile(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"simple", "big", "multi-sources"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "Gemfile.lock")
				got, _, err := Parse(fixture, parser.Options{})
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc, got)
			})
		}
	})
}
