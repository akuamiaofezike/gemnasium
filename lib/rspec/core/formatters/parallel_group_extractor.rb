# ParallelGroupExtractor is an rspec formatter that simply lists the locations
# of context groups where :run_parallel is set to true.
#
# To list parallel groups, run rspec in dry-run mode using that formatter.
# This generates a list of locations (file paths and line numbers)
# you can pass back to rspec, to execute these parallel groups.
#
# If the environment variables CI_NODE_TOTAL and CI_NODE_INDEX are set,
# it only lists the locations that are in the selected bucket.
#
class ParallelGroupExtractor
  RSpec::Core::Formatters.register self, :example_group_started, :close

  def initialize(output)
    @output = output
    @locations = []
  end

  # Store the location of a group where :run_parallel
  # is explicitly set to true.
  #
  # Since group metadata is inheritied, a group is skipped
  # if its parent has :run_parallel set to true.
  # Shared groups are also skipped.
  #
  def example_group_started(notification)
    metadata = notification.group.metadata
    return unless metadata[:run_parallel]
    return if metadata[:shared_group_name]

    parent = notification.group.metadata[:parent_example_group]
    return if parent[:run_parallel]

    @locations << metadata[:location]
  end


  # Show the locations that are in the selected bucket
  def close(notification)
    @output.puts select_locations(@locations).join(" ")
  end

  private

  # Select locations that are in the selected bucket,
  # or all location if there's only one bucket
  def select_locations(all)
    return all if num_of_buckets == 1

    # approximate bucket size
    bucket_size = all.length / num_of_buckets

    # maximum bucket index
    max_bucket_idx = num_of_buckets - 1

    # select locations with a rank that matches
    # the selected bucket index
    selected = []
    all.each_with_index do |location, i|
      bucket_idx = [i / bucket_size, max_bucket_idx].min
      if bucket_idx == selected_bucket_idx
        selected << location
      end
    end
    return selected
  end

  def selected_bucket_idx
    ENV.fetch("CI_NODE_INDEX", 1).to_i - 1
  end


  def num_of_buckets
    ENV.fetch("CI_NODE_TOTAL", 1).to_i
  end
end
